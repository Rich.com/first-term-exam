package rw.ac.rca.termOneExam.controller;


import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.dto.CreateCityDTO;
import rw.ac.rca.termOneExam.utils.APICustomResponse;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CityControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getById_Success() {
        City city = this.restTemplate.getForObject("/api/cities/id/101", City.class);
        assertEquals("Kigali", city.getName());
    }

    @Test
    public void getById_404(){
        City city = this.restTemplate.getForObject("/api/cities/id/106", City.class);
        assertNull(city.getName());
    }

    @Test
    public void getAll_success() throws JSONException {
        String response = this.restTemplate.getForObject("/api/cities/all", String.class);
        JSONAssert.assertEquals("[{id: 101},{id: 102},{id: 103},{id: 104}]",response,false);
    }

    @Test
    public void addCity_success(){
        CreateCityDTO dto = new CreateCityDTO("Bern",18);
        ResponseEntity<APICustomResponse> city = this.restTemplate.postForEntity("/api/cities/add",dto,APICustomResponse.class);
        assertTrue(city.getStatusCode().is2xxSuccessful());
    }

    @Test
    public void addCity_fail(){
        CreateCityDTO dto = new CreateCityDTO("Kigali",18);
        ResponseEntity<APICustomResponse> city = this.restTemplate.postForEntity("/api/cities/add",dto,APICustomResponse.class);
        assertEquals(400,city.getStatusCodeValue());
    }

}
