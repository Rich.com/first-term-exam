package rw.ac.rca.termOneExam.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.dto.CreateCityDTO;
import rw.ac.rca.termOneExam.repository.ICityRepository;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {
    @Mock
    private ICityRepository cityRepositoryMock;

    @InjectMocks
    private CityService cityService;

    @Test
    public void getById_success(){
        City city = new City("Kigali",27);
        when(cityRepositoryMock.findById(1L)).thenReturn(Optional.of(city));
        assertEquals("Kigali", cityService.getById(1).get().getName());
    }

    @Test
    public void getById_fail(){
        when(cityRepositoryMock.findById(1L)).thenReturn(null);
        assertNull(cityService.getById(1));
    }

    @Test
    public void getAll_success(){
        when(cityRepositoryMock.findAll()).thenReturn(Arrays.asList(new City("Kigali", 27), new City("Bern",18)));
        assertEquals("Bern", cityService.getAll().get(1).getName());
    }

    @Test
    public void addCity_success(){
        CreateCityDTO dto = new CreateCityDTO("Bern", 18);
        City city = new City(dto.getName(),dto.getWeather());
        when(cityService.save(dto)).thenReturn(city);
        assertEquals("Bern", city.getName());
    }

    @Test
    public void existsByName_true(){
        when(cityRepositoryMock.existsByName("Kigali")).thenReturn(true);
        assertTrue(cityService.existsByName("Kigali"));
    }

    @Test
    public void existsByName_false(){
        when(cityRepositoryMock.existsByName("Kigali")).thenReturn(false);
        assertFalse(cityService.existsByName("kigali"));
    }

}
