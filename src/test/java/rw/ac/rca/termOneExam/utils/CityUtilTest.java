package rw.ac.rca.termOneExam.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.repository.ICityRepository;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.any;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CityUtilTest {

    @Mock
    private ICityRepository cityRepository;
    @Test
    public void noWeatherMoreThan40() {
        List<City> cities = new ArrayList<>();
        cities.add(new City("Kigali",28));
        cities.add(new City("Musanze", 20));

        assertFalse(isGreaterThan40(cities));
    }



    public boolean isGreaterThan40(List<City> cities){
        for (City city : cities) {
            if(city.getWeather() > 40){
                return true;
            }
        }
        return false;
    }


    @Test
    public void noCityHasWeatherLessThan10(){
        List<City> cities = new ArrayList<>();
        cities.add(new City("Kigali",28));
        cities.add(new City("Musanze", 20));

        assertFalse(isLessThan10(cities));
    }

    private boolean isLessThan10(List<City> cities) {
        for (City city : cities) {
            if(city.getWeather() < 10){
                return true;
            }
        }
        return false;
    }

    @Test
    public void citiesContainKigaliAndMusanze(){
        List<City> cities = new ArrayList<>();
        cities.add(new City("Kigali",28));
        cities.add(new City("Musanze", 20));
        assertTrue(areMusanzeAndKigaliIncluded(cities));
    }

    private boolean areMusanzeAndKigaliIncluded(List<City> cities){
        boolean musanze = false;
        boolean kigali = false;

        for (City city : cities){
            if(city.getName().equals("Kigali") ){
                kigali = true;
            }
            if(city.getName().equals("Musanze")){
                musanze = true;
            }
        }
        return kigali && musanze;
    }

    @Test
    public void spyingOnList_success() {

        City city1 = new City("Kigali", 28);
        City city2 = new City("Musanze", 20);

        List<City> spyCities = spy(List.class);
        assertEquals(spyCities.size(), 0);
        assertNull(spyCities.get(0));
        spyCities.add(city1);
        assertEquals(spyCities.size(), 0);
        when(spyCities.size()).thenReturn(5);
        assertEquals(spyCities.size(), 5);

        spyCities.add(city2);
        assertEquals(spyCities.size(), 5);
    }

    @Test
    public void whenCreateMock_thenCreated() {
        ArrayList<City> mockedList = Mockito.mock(ArrayList.class);

        City city1 = new City("Kigali", 28);
        mockedList.add(city1);
        verify(mockedList).add(city1);

        assertEquals(0, mockedList.size());
    }
}
